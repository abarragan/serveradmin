package com.moebel.serveradmin.domain;

import org.junit.Assert;
import org.junit.Test;

public class VirtualMachineTest {

    @Test
    public void vmFitsServerTest() {

        Server server = new Server(2, 32, 100);
        VirtualMachine virtualMachine = new VirtualMachine(2, 32, 100);

        boolean result = virtualMachine.fits(server);

        Assert.assertTrue(result);
    }

    @Test
    public void vmFitsServer2Test() {

        Server server = new Server(2, 32, 100);
        VirtualMachine virtualMachine = new VirtualMachine(1, 8, 5);

        boolean result = virtualMachine.fits(server);

        Assert.assertTrue(result);
    }

    @Test
    public void vmDoesNotFitServerTest() {

        Server server = new Server(2, 32, 100);
        VirtualMachine virtualMachine = new VirtualMachine(3, 32, 200);

        boolean result = virtualMachine.fits(server);

        Assert.assertFalse(result);
    }
}
