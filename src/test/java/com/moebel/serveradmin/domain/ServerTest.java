package com.moebel.serveradmin.domain;

import org.junit.Assert;
import org.junit.Test;

public class ServerTest {
    @Test
    public void shouldSubtractTest() {
        VirtualMachine virtualMachine = new VirtualMachine(1, 8, 10);

        Server server = new Server(2, 32, 100);

        server.subtractResources(virtualMachine);

        Assert.assertEquals(1, server.getCpu());
        Assert.assertEquals(24, server.getRam());
        Assert.assertEquals(90, server.getHdd());
    }
}
