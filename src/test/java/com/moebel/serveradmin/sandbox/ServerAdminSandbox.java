package com.moebel.serveradmin.sandbox;

import com.moebel.serveradmin.ServerAdmin;
import com.moebel.serveradmin.domain.Server;
import com.moebel.serveradmin.domain.VirtualMachine;

import java.util.ArrayList;
import java.util.List;

public class ServerAdminSandbox {

    public static void main(String[] args) {
        Server server = new Server(2, 32, 100);
        List<VirtualMachine> virtualMachines = new ArrayList<>();

        virtualMachines.add(new VirtualMachine(1, 16, 10));
        virtualMachines.add(new VirtualMachine(2, 32, 100));
        virtualMachines.add(new VirtualMachine(1, 16, 10));


        ServerAdmin serverAdmin = new ServerAdmin();
        int result = -1;
        try {
            result = serverAdmin.calculate(server, virtualMachines.toArray(new VirtualMachine[0]));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("*****AMOUNT OF SERVERS NEEDED: " + result);
    }
}
