package com.moebel.serveradmin;

import com.moebel.serveradmin.domain.Server;
import com.moebel.serveradmin.domain.VirtualMachine;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ServerAdminTest {

    @Test
    public void calculateTest() throws CloneNotSupportedException {
        Server server = new Server(2, 32, 100);
        List<VirtualMachine> virtualMachines = new ArrayList<>();

        virtualMachines.add(new VirtualMachine(1, 16, 10));
        virtualMachines.add(new VirtualMachine(2, 32, 100));
        virtualMachines.add(new VirtualMachine(1, 16, 10));

        ServerAdmin serverAdmin = new ServerAdmin();
        int result = serverAdmin.calculate(server, virtualMachines.toArray(new VirtualMachine[0]));

        Assert.assertEquals(3, result);
    }

    @Test
    public void calculate2Test() throws CloneNotSupportedException {
        Server server = new Server(2, 32, 100);
        List<VirtualMachine> virtualMachines = new ArrayList<>();

        virtualMachines.add(new VirtualMachine(2, 32, 100));
        virtualMachines.add(new VirtualMachine(1, 16, 10));
        virtualMachines.add(new VirtualMachine(1, 16, 10));

        ServerAdmin serverAdmin = new ServerAdmin();
        int result = serverAdmin.calculate(server, virtualMachines.toArray(new VirtualMachine[0]));

        Assert.assertEquals(2, result);
    }

    @Test
    public void calculateEmptyListVMTest() throws CloneNotSupportedException {
        Server server = new Server(2, 32, 100);

        ServerAdmin serverAdmin = new ServerAdmin();
        int result = serverAdmin.calculate(server, new ArrayList<>().toArray(new VirtualMachine[0]));

        Assert.assertEquals(0, result);
    }

    @Test
    public void calculateAllNonFittingTest() throws CloneNotSupportedException {
        Server server = new Server(2, 32, 100);
        List<VirtualMachine> virtualMachines = new ArrayList<>();

        virtualMachines.add(new VirtualMachine(1, 32, 1000));
        virtualMachines.add(new VirtualMachine(4, 32, 50));
        virtualMachines.add(new VirtualMachine(5, 128, 100));

        ServerAdmin serverAdmin = new ServerAdmin();
        int result = serverAdmin.calculate(server, virtualMachines.toArray(new VirtualMachine[0]));

        Assert.assertEquals(0, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateShouldThrowIllegalArgumentExceptionNullServerTest() throws CloneNotSupportedException {

        List<VirtualMachine> virtualMachines = new ArrayList<>();

        virtualMachines.add(new VirtualMachine(2, 32, 100));
        ServerAdmin serverAdmin = new ServerAdmin();
        serverAdmin.calculate(null, virtualMachines.toArray(new VirtualMachine[0]));

        Assert.fail("Illegal exception wasn't thrown on null argument");
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateShouldThrowIllegalArgumentExceptionNullVMsTest() throws CloneNotSupportedException {

        Server server = new Server(2, 32, 100);

        ServerAdmin serverAdmin = new ServerAdmin();
        serverAdmin.calculate(server, null);

        Assert.fail("Illegal exception wasn't thrown on null argument");
    }
}
