package com.moebel.serveradmin.domain;

public class VirtualMachine {

    private int cpu;
    private int ram;
    private int hdd;

    public VirtualMachine(int cpu, int ram, int hdd) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
    }

    public int getCpu() {
        return cpu;
    }

    public int getRam() {
        return ram;
    }

    public int getHdd() {
        return hdd;
    }

    public boolean fits(Server server) {
        if (server.getCpu() - cpu >= 0)
            if (server.getRam() - ram >= 0)
                return server.getHdd() - hdd >= 0;
        return false;
    }
}
