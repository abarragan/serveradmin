package com.moebel.serveradmin.domain;

public class Server implements Cloneable {

    private int cpu;
    private int ram;
    private int hdd;

    public Server(int cpu, int ram, int hdd) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
    }

    public int getCpu() {
        return cpu;
    }

    public int getRam() {
        return ram;
    }

    public int getHdd() {
        return hdd;
    }

    /**
     * Updates server with remaining resources it has after having allocated given VM
     *
     * @param virtualMachine - VM to be allocated in the sample server
     */
    public void subtractResources(VirtualMachine virtualMachine) {
        cpu = cpu - virtualMachine.getCpu();
        ram = ram - virtualMachine.getRam();
        hdd = hdd - virtualMachine.getHdd();
    }

    public Server clone() throws CloneNotSupportedException {
        return (Server) super.clone();
    }
}
