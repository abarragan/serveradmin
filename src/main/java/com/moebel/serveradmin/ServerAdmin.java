package com.moebel.serveradmin;

import com.moebel.serveradmin.domain.Server;
import com.moebel.serveradmin.domain.VirtualMachine;

public class ServerAdmin {

    /**
     * Calculates the amount of servers needed for a given list of VMs with a specified type of server
     *
     * @param serverType      - the server type to be used as reference
     * @param virtualMachines - list of virtual machines to allocate
     * @return amount of servers to use
     * @throws CloneNotSupportedException
     */
    public int calculate(Server serverType, VirtualMachine[] virtualMachines) throws CloneNotSupportedException {

        if (serverType == null || virtualMachines == null)
            throw new IllegalArgumentException("Neither `server` or `virtualMachines` can be null");

        if (virtualMachines.length == 0)
            return 0;

        int result = 1;
        boolean allNonFitting = true;
        Server sample = serverType.clone();

        for (VirtualMachine virtualMachine : virtualMachines) {
            if (virtualMachine.fits(serverType)) {
                if (virtualMachine.fits(sample)) {
                    allNonFitting = false;
                } else {
                    result++;
                    sample = serverType.clone();
                }
                sample.subtractResources(virtualMachine);
            }
        }
        return allNonFitting ? 0 : result;
    }
}
